#ifndef ELO_SCORE
#define ELO_SCORE

#include <iostream>
#include <cmath>
#include "EScore.h"

EScore::EScore(double K,int Ra,int Rb):K(K),Ra(Ra),Rb(Rb){
};

double EScore::getK(){return K;};
int EScore::getRa(){return Ra;};
int EScore::getRb(){return Rb;};

void EScore::setK(double t){K=t;};
void EScore::setRa(int t){Ra=t;};
void EScore::setRb(int t){Rb=t;};

void EScore::gameResult(double r){
	double Ea=1/(1+pow(10,(double)(Rb-Ra)/400));
	double Eb=1/(1+pow(10,(double)(Ra-Rb)/400));

	//Only left 3 significant number
	Ea=(double)(round(Ea*1000))/1000;
	Eb=(double)(round(Eb*1000))/1000;

	cout<<"Ea="<<Ea<<" Eb="<<Eb<<endl;
	Ra=round((double)Ra+K*(r-Ea));
	Rb=round((double)Rb+K*(1-r-Eb));
};

void EScore::printScore(){
	cout<<Ra<<" "<<Rb<<endl;
}

void EScore::game(double r){
	gameResult(r);
	printScore();
}

#endif
