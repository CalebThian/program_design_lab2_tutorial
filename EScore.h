#ifndef ELO_SCORE_H
#define ELO_SCORE_H

#include <iostream>
using namespace std;

class EScore{
	public:
		EScore(double K,int Ra,int Rb);
		double getK();
		int getRa();
		int getRb();
		void setK(double t);
		void setRa(int t);
		void setRb(int t);
		void gameResult(double r);
		void printScore();
		void game(double r);
	private:
		double K;
		int Ra;
		int Rb;
};


#endif
