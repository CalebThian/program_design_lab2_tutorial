lab2: EScore.o lab2.cpp
	g++ -o lab2 lab2.cpp EScore.o

EScore.o: EScore.cpp EScore.h
	g++ -c EScore.cpp

clean:
	rm lab2 *.o
