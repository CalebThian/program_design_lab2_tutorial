#include <iostream>
#include <fstream>
#include <cstdlib>
#include "EScore.h"
using namespace std;

int main(){
	double K,Ra,Rb;

	//set up input file
	ifstream inFile;
	inFile.open("file.in",ios::in);
	if(!inFile){
		cerr<<"Failed opening infile"<<endl;
		exit(1);
	}

	//initialize EScore system
	inFile>>K>>Ra>>Rb;
	EScore system(K,Ra,Rb);

	//set up output file
	ofstream outFile;
	outFile.open("file.out",ios::out);
	if(!outFile){
		cerr<<"Failed opening outfile"<<endl;
		exit(2);
	}
	
	//read in score
	double s;
	outFile<<system.getRa()<<" "<<system.getRb()<<endl;
	while(inFile>>s){
		system.gameResult(s);
		outFile<<system.getRa()<<" "<<system.getRb()<<endl;
	}


	return 0;
}
